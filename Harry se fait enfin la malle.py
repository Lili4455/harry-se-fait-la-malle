fournitures_scolaires = \
[{'Nom' : 'Manuel scolaire', 'Poids' : 0.55, 'Mana' : 11},
{'Nom' : 'Baguette magique', 'Poids' : 0.085, 'Mana' : 120},
{'Nom' : 'Chaudron', 'Poids' : 2.5, 'Mana' : 2},
{'Nom' : 'Boîte de fioles', 'Poids' : 1.2, 'Mana' : 4},
{'Nom' : 'Téléscope', 'Poids' : 1.9, 'Mana' : 6},
{'Nom' : 'Balance de cuivre', 'Poids' : 1.3, 'Mana' : 3},
{'Nom' : 'Robe de travail', 'Poids' : 0.5, 'Mana' : 8},
{'Nom' : 'Chapeau pointu', 'Poids' : 0.7, 'Mana' : 9},
{'Nom' : 'Gants', 'Poids' : 0.6, 'Mana' : 25},
{'Nom' : 'Cape', 'Poids' : 1.1, 'Mana' : 13}]

POIDS_MAXIMAL = 4

def remplissage_sans_optimisation(POIDS_MAX, fournitures):
    poids_actuel = 0
    fournitures_choisies = []
    for objet in fournitures:
        if objet['Poids'] + poids_actuel <= POIDS_MAX:
            fournitures_choisies.append(objet)
            poids_actuel += objet['Poids']
    return fournitures_choisies

def glouton(POIDS_MAX, fournitures, critere):
    fournitures = tri(fournitures, critere)[::-1]
    fournitures_choisies = []
    poids_malle = 0

    for objet in fournitures:
        if poids_malle + objet['Poids'] <= POIDS_MAX:
            poids_malle += objet['Poids']
            fournitures_choisies.append(objet)

    return fournitures_choisies

def brut_force(max, fournitures):
    '''
    Bonus
    '''
    def calcul_combinaisons(nombre_objets_dans_la_combinaison, iterable):
        if not nombre_objets_dans_la_combinaison:
            return [[]]
        if not iterable:
            return []

        debut = [iterable[0]]
        suite = iterable[1:]
        combinaison = [ debut + i for i in calcul_combinaisons(nombre_objets_dans_la_combinaison - 1, suite) ]


        return combinaison + calcul_combinaisons(nombre_objets_dans_la_combinaison, suite)
    
    # Best est un entier contenant le meilleur poids obtenu afin de le comparer
    # result est une liste contenant la meilleure combinaison
    best = 0
    result = []

    somme = 0
    liste = [i['Poids'] for i in fournitures]
    for i in range(1, len(liste) + 1):
        for j in calcul_combinaisons(i, liste):
            somme = sum(j)
            if somme <= max and somme > best:
                best = somme
                result = j
                if somme == max:
                    break
    if result:
        return [j for j in fournitures for i in result if j['Poids'] == i]
    return

def tri(liste, critere):
    # Pas besoin de trier une lise d'un seul élément ou moins
    if len(liste) < 2:
        return liste

    # On utilise .pop() pour retirer le pivot de la liste ([-1] le ferait
    # rester dans la liste) et on économise des tours pour trier
    pivot = liste.pop()

    # On crée les deux partitions
    superieur = []
    inferieur = []

    # On remplie les partitions
    for objet in liste:
        if objet[critere] <= pivot[critere]:
            inferieur.append(objet)
        else:
            superieur.append(objet)
    
    # Tant que toute les partitions ne sont pas triées on ne renvoie rien
    # Une fois cela fait on renvoie la liste triée
    return tri(inferieur, critere) + [pivot] + tri(superieur, critere)




def affichage(fournitures_choisies):
    somme_poids = sum(i['Poids'] for i in fournitures_choisies)
    somme_mana = sum(i['Mana'] for i in fournitures_choisies)
    fournitures_choisies = [i['Nom'] for i in fournitures_choisies]
    print(f"\nHarry a rempli sa malle, voici ce qu'il a choisi :\n"
            f"{fournitures_choisies}\n"
            f"Le poids total de la malle est : {somme_poids:.3f}\n"
            f"Le mana total de la malle est : {somme_mana}\n")



def ihm(message):
    """
    Entrée : message à écrire a début de la fonction

    Cette fonction permet à l'utilisateur de choisir les fonctions à exécuter de manière simple grâce au clavier.
    """
    if message:
        print(message)

    # On empêche l'utilisateur de faire une saisie invalide
    try:
        # Il est nécessaire de slicer les fournitures_scolaires pour ne pas les
        # modifier avec la méthode .pop() dans la fonction tri()
        # c'est parce que le paramètre <liste> de la fonction tri() pointerait sur la
        # liste qui est contenue dans la variable globale fournitures_scolaires
        choix = int(input('( 1 = Rapide ; 2 = Optimisé en fonction du poids ; 3= Optimisé en fonction du Mana ; 4 = Optimlisé en fonction du poids par force brute )\n'))

        if not choix in (1,2,3,4):
            raise
        elif choix == 1:
            affichage(remplissage_sans_optimisation(POIDS_MAXIMAL, fournitures_scolaires[:]))
        elif choix == 2:
            affichage(glouton(POIDS_MAXIMAL, fournitures_scolaires[:], 'Poids'))
        elif choix == 3:
            affichage(glouton(POIDS_MAXIMAL, fournitures_scolaires[:], 'Mana'))
        else:
            affichage(brut_force(POIDS_MAXIMAL, fournitures_scolaires))
    except:
        ihm(f"<{choix}> n'est pas valide. Veuillez réessayer")

    if input('Voulez-vous essayer un nouveau mode de remplissage ? (Y) : ').lower() == "y":
        ihm('Comment Harry va-t-il remplir sa malle cette fois-ci ?')

ihm('Bonjour et bienvenue dans notre remplisseur de malle automatique !\nComment Harry va-t-il remplir sa malle ?')
